from datetime import datetime
import argparse
import sys

from pyramid.paster import bootstrap, setup_logging
from sqlalchemy.exc import OperationalError


from ..models.nbu import (
    Bank,
    NBUExchange,
    )


def setup_models(db):
    """
    Add or update models / fixtures in the database.

    """
    today = datetime.now().strftime('%d.%m.%Y')
    bank_list = (
        Bank(
            name='PB',
            endpoint='https://api.privatbank.ua/p24api/exchange_rates',
        ),
        Bank(
            name='NBU',
            endpoint='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange',
        ),
    )
    nbu_list = (
        NBUExchange(
            **{
                "r030": 978,
                "txt": "Євро",
                "rate": 27.242936,
                "cc": "EUR",
                "exchangedate": today
              },
        ),
        NBUExchange(
            **{
                "r030": 840,
                "txt": "Долар США",
                "rate": 24.877122,
                "cc": "USD",
                "exchangedate": today
              },
        ),
        NBUExchange(
            **{
                "r030": 643,
                "txt": "Російський рубль",
                "rate": 0.38195,
                "cc": "RUB",
                "exchangedate": today
              }
        ),
    )
    for bank in bank_list:
        db.add(bank)
    for nbu_exch in nbu_list:
        db.add(nbu_exch)


def drop_models(db):
    db.query(Bank).delete()
    db.query(NBUExchange).delete()


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config_uri',
        help='Configuration file, e.g., development.ini',
    )
    parser.add_argument(
        '-m', '--mode',
        choices=('setup', 'drop'),
        default='setup',
        help='Setup or drop model',
    )
    return parser.parse_args(argv[1:])


def main(argv=sys.argv):
    args = parse_args(argv)
    setup_logging(args.config_uri)
    env = bootstrap(args.config_uri)
    mode = args.mode
    try:
        with env['request'].tm:
            db = env['request'].db
            if mode == 'setup':
                setup_models(db)
            else:
                drop_models(db)
    except OperationalError:
        print('''
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:
1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for description and try to run it.
2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.
            ''')
