from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    with Configurator(settings=settings) as config:
        config.include('pyramid_jinja2')
        config.include('pyramid_rpc.jsonrpc')
        config.include('cornice')
        config.include('pyramid_celery')
        config.configure_celery('development.ini')

        config.include('.api')
        config.include('.models')
        config.include('.routes')
        config.scan('.views')
    return config.make_wsgi_app()
