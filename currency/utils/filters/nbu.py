from currency.models.nbu import NBUExchange


def filter_nbu_exchange(request, **kwargs):
    query = request.db.query(NBUExchange).filter_by(
        exchangedate=request.validated['date'],
    )
    if request.validated.get('valcode'):
        query = query.filter_by(cc=request.validated['valcode'])
    return query.all()
