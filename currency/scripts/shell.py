import sys
import argparse

from pyramid.paster import bootstrap, setup_logging

from traitlets.config.loader import Config
from IPython import start_ipython


from ..models.nbu import (
    Bank,
    NBUExchange,
    )


def import_models():
    return {
        'Bank': Bank,
        'NBUExchange': NBUExchange,
    }


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config_uri',
        help='Configuration file, e.g., development.ini',
    )
    return parser.parse_args(argv[1:])


def ipython(**locals_):
    start_ipython(
        argv=[],
        user_ns=locals_,
        config=Config(
            HistoryManager={
                'enabled': True,
            },
            TerminalInteractiveShell={
                'confirm_exit': False,
                'term_title': False,
            },
            TerminalIPythonApp={
                'display_banner': False,
            },
        ),
    )


def main(argv=sys.argv):
    args = parse_args(argv)
    setup_logging(args.config_uri)
    env = bootstrap(args.config_uri)
    with env['request'].tm:
        db = env['request'].db
        ipython(
            env=env,
            db=db,
            **import_models(),
        )
