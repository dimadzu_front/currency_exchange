from datetime import datetime
import logging

from pyramid_rpc.jsonrpc import jsonrpc_method, JsonRpcParamsInvalid
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
from pyramid.view import (
    view_config,
    view_defaults,
)

from ..models.nbu import Bank, NBUExchange


log = logging.getLogger(__name__)


@jsonrpc_method(endpoint='rpc')
def test(request):
    return [1, 2, 3]


@view_defaults(renderer='templates/index.jinja2')
class IndexViews:
    def __init__(self, request):
        self.request = request

    @view_config(route_name='index')
    def index(self):
        banks = self.request.db.query(Bank).all()
        return {'banks': banks}


@view_defaults(renderer='templates/nbu.jinja2')
class NbuViews:
    def __init__(self, request):
        self.request = request

    @view_config(route_name='nbu')
    def nbu(self):
        today = datetime.now().replace(
                    hour=0,
                    minute=0,
                    second=0,
                    microsecond=0,
                )
        return {
            'nbu': self.request.db.query(
                NBUExchange,
            ).filter_by(
                exchangedate=today,
            ).all(),
        }
