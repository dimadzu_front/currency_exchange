from currency.validators.nbu import (
    validate_date,
    validate_valcode,
)
from currency.utils.filters.nbu import filter_nbu_exchange
from cornice import Service


nbu_exchange = Service(name='nbu_exchange',
                       path='/api/NBUStatService/v1/statdirectory/exchange',
                       description='NBU Exchange rates')


@nbu_exchange.get(validators=(
        validate_date,
        validate_valcode,
))
def get_nbu_exchange_rate(request):
    """Returns the NBU exchange rate.
    """
    return filter_nbu_exchange(request)
