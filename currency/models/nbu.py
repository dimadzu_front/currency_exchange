import datetime

from pyramid.security import Allow, Everyone

from sqlalchemy import (
    Column,
    Integer,
    Float,
    Text,
    DateTime,
    String,
    )


from .meta import DBSession, Base

from currency.schema.nbu import (
    NBUExchangeSchema,
)


class Bank(Base):
    __tablename__ = 'banks'
    pk = Column(Integer, primary_key=True)
    name = Column(Text, unique=True, nullable=False)
    endpoint = Column(Text, nullable=False)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return 'Bank(pk=%s, name=%s, endpoint=%s, created=%s)' % (
            self.pk,
            self.name,
            self.endpoint,
            self.created,
        )

    def __str__(self):
        return 'Bank(pk=%s, name=%s, endpoint=%s, created=%s)' % (
            self.pk,
            self.name,
            self.endpoint,
            self.created,
        )


class NBUExchange(Base):
    __tablename__ = 'nbu_exchanges'
    pk = Column(Integer, primary_key=True)
    r030 = Column(Integer, nullable=False)
    txt = Column(Text, nullable=False)
    rate = Column(Float(precision=7), nullable=False)
    cc = Column(String(10), nullable=False)
    exchangedate = Column(DateTime, nullable=False)

    def __repr__(self):
        return 'NBUExchange(pk=%s, r030=%s, txt=%s, rate=%s, cc=%s, exchangedate=%s)' % (
            self.pk,
            self.r030,
            self.txt,
            self.rate,
            self.cc,
            self.exchangedate,
        )

    def __str__(self):
        return 'NBUExchange(pk=%s, r030=%s, txt=%s, rate=%s, cc=%s, exchangedate=%s)' % (
            self.pk,
            self.r030,
            self.txt,
            self.rate,
            self.cc,
            self.exchangedate,
        )

    def __json__(self, request=None):
        return NBUExchangeSchema().dump(self)


class Root(object):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, 'group:editors', 'edit')]

    def __init__(self, request):
        pass
